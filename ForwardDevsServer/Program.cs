﻿using SuperWebSocket;
using System;

namespace ForwardDevsServer
{
    class Program
    {
        // Variables a definir
        private static WebSocketServer wsServer;
        private static string version = "0.1.0";
        private static string connectionString;

        // Esta es la función que se ejecuta nada mas iniciar el servicio
        static void Main(string[] args)
        {
            // Conectamos con la base de datos
            string host = "localhost";
            string user = "root";
            string pass = "";
            string db = "fd";
            connectionString = "SERVER=" + host + "; DATABASE=" + db + "; UID=" + user + "; PASSWORD=" + pass + "; SSLMODE=none;";
            Console.WriteLine("[INFO] Conexión con la base de datos realizada con éxito");

            // Iniciamos el servidor websockets
            Http.sendWebHook(4, "🔔 ¡El servidor de ForwardDevs se ha iniciado con éxito! ("+ version + ")");
            wsServer = new WebSocketServer();
            int port = 8088;
            wsServer.Setup(port);
            wsServer.NewSessionConnected += WsServer_NewSessionConnected;
            wsServer.NewMessageReceived += WsServer_NewMessageReceived;
            wsServer.NewDataReceived += WsServer_NewDataReceived;
            wsServer.SessionClosed += WsServer_SessionClosed;
            wsServer.Start();
            Console.WriteLine("ForwardDevs Main Server | v" + version);
            Console.WriteLine("Puerto actual: " + port);
            Console.WriteLine("Pulsa cualquier tecla para salir");
            Console.ReadKey();
            // A partir de aquí lo que pasa cuando el servidor se apaga (si lo crasheas esto no se ejecuta)
            Http.sendWebHook(4, "⛔️ ¡El servidor de ForwardDevs se ha apagado!");
        }

        // Función para poder usar mysql en otra clase
        public static string GetConnectionString()
        {
            return connectionString;
        }

        // Función que define lo que pasa cuando una sesión se cierra
        private static void WsServer_SessionClosed(WebSocketSession session, SuperSocket.SocketBase.CloseReason value)
        {
            Console.WriteLine("Sesión cerrada");
        }

        // Función que define lo que pasa cuando un byte nuevo se recibe
        private static void WsServer_NewDataReceived(WebSocketSession session, byte[] value)
        {
            Console.WriteLine("Nuevo dato recibido");
        }

        // Función que define lo que pasa cuando un string nuevo se recibe
        private static void WsServer_NewMessageReceived(WebSocketSession session, string value)
        {
            if (Database.Main.CheckIfLicenseExists(value))
            {
                Http.sendWebHook(2, "✅ Un cliente ha usado ForwardShop con una licencia válida.");
                session.Send("OK");
            }
            else
            {
                Http.sendWebHook(2, $"⚠**INFORMACIÓN** Se ha intentado abrir ForwardShop con una licencia inválida. | Origen: *{session.Origin}* | @everyone");
                session.Send("NO");
            }
        }

        // Función que define lo que pasa cuando una sesión se conecta
        private static void WsServer_NewSessionConnected(WebSocketSession session)
        {
            if (!Database.Main.CheckIfHostIsValid(session.Origin))
            {
                Http.sendWebHook(4, $"⚠**INFORMACIÓN** Se ha rechazado una conexión con el servidor de WebSockets desde un host no existente en nuestra base de datos | Origen: *{session.Origin}* | Fecha: *{session.LastActiveTime}* @everyone");
                session.Close();
                return;
            }

            session.Send("OK");
            Http.sendWebHook(4, $"✅ Nueva sesión en el servidor de WebSockets | Origen: {session.Origin} | Fecha: {session.LastActiveTime}");
        }
    }
}