﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace ForwardDevsServer
{
    class Mail
    {
        // Variables
        private static SmtpClient SmtpServer = new SmtpClient("smtp.ionos.es"); // Servidor SMTP

        public static void SendTestMail()
        {
            // Usamos try / catch para poder loggear si hay algún bug
            try
            {
                // Configuración del servidor de correo
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@muphy.es", "CacaHueteJEJE66667");
                SmtpServer.EnableSsl = true;
                SmtpServer.Port = 587;
                MailMessage mail = new MailMessage();

                // Configuración del mensaje
                mail.From = new MailAddress("contacto@forwarddevs.com");
                mail.To.Add("contacto@muphy.es");
                mail.Subject = "Test Mail";
                mail.Body = "This is for testing SMTP mail from GMAIL";

                // Enviamos el mail
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                // Enviamos el log a discord
                Http.sendWebHook(1, ex.ToString());
            }
        }
    }
}
