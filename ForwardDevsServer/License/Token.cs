﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForwardDevsServer.License
{
    class Token
    {
        private static Random random = new Random();
        public static string GenerateLicenseKey(int length)
        {
            try
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                string key = new string(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());

                if (Database.Main.CheckIfLicenseExists(key))
                {
                    key = new string(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
                }

                string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                Database.Main.SaveLicenseKey(key, date);

                Http.sendWebHook(2, "🔊 Se ha generado un nueva License Key en el servidor principal: **" + key + "**");
                Http.sendWebHook(2, "📅 **" + date + "**");
                return key;
            }
            catch(Exception ex)
            {
                Http.sendWebHook(1, ex.ToString());
                return "no";
            }
        }
    }
}
