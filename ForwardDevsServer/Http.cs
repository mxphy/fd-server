﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace ForwardDevsServer
{
    class Http
    {
        // Funcion para crear un weclient 
        public static byte[] Post(string url, NameValueCollection pairs)
        {
            using (WebClient webClient = new WebClient())
                return webClient.UploadValues(url, pairs);
        }

        // Función para enviar el webhook
        public static void sendWebHook(int type, string message)
        {
            string url = "";
            string username = "";
            switch (type)
            {
                case 1:
                    url = "https://discordapp.com/api/webhooks/594972626967986177/DPFuKHl4VxpTt_4RETBJFqzV2uahEVEudqVBP--0mWCJCRca6_8TA23Ww3TB_cGtxdeE";
                    username = "Server | Exception";
                    break;

                case 2:
                    url = "https://discordapp.com/api/webhooks/594983291254210620/EcQwl_Mbx9UnmCHjIrXlmkVLFOXUfbMEMOKnFhUCBBDET7JdwPFH9fNw0bgvT5L_28Ge";
                    username = "Server | License Key";
                    break;

                case 3:
                    url = "https://discordapp.com/api/webhooks/561648142479654970/cWenTDzGN_eFO2h8gP9aT8tF6qKViUKWTCKKCnl5jH32gHtoqnB8t5Cuy01oiT-wHdXC";
                    username = "Server | General";
                    break;

                case 4:
                    url = "https://discordapp.com/api/webhooks/595372652433244201/s0KTjiiPRrRWP2XgePi8LicHJiaA5JJ8YAxL842oPMSFtcOKLgTGAcOoRvPhteUp1cAx";
                    username = "Server | Avisos";
                    break;
            }

            Http.Post(url, new NameValueCollection()
            {
                {
                    "username",
                    username
                },
                {
                    "content",
                    message
                }
            });
        }
    }
}
