﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient; 

namespace ForwardDevsServer.Database
{
    class Main
    {
        public static void SaveLicenseKey(string license, string date)
        {
            using (MySqlConnection connection = new MySqlConnection(Program.GetConnectionString()))
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "INSERT INTO licenses (token, date, status) VALUES (@license, @date, 0)";
                command.Parameters.AddWithValue("@license", license);
                command.Parameters.AddWithValue("@date", date);

                command.ExecuteNonQuery();
            }
        }

        public static bool CheckIfLicenseExists(string token)
        {
            bool checkLicense = false;
            using (MySqlConnection connection = new MySqlConnection(Program.GetConnectionString()))
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM licenses WHERE token = @token";
                command.Parameters.AddWithValue("@token", token);
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    checkLicense = reader.HasRows;
                }
                return checkLicense;
            }
        }

        public static bool CheckIfHostIsValid(string host)
        {
            bool checkHost = false;
            using (MySqlConnection connection = new MySqlConnection(Program.GetConnectionString()))
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM hosts WHERE host = @host AND allowed = 1";
                command.Parameters.AddWithValue("@host", host);
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    checkHost = reader.HasRows;
                }
                return checkHost;
            }
        }
    }
}
